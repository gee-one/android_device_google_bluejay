# local changes for low effort OS

# flag for gapps
ifeq ($(WITH_GMS), true)
    $(call inherit-product-if-exists, vendor/gapps/arm64/arm64-vendor.mk)
    $(call inherit-product-if-exists, vendor/gapps/common/common-vendor.mk)
endif

# add gesture typing libs
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/libjni_latinimegoogle.so:product/lib64/libjni_latinimegoogle.so
